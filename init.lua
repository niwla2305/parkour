-- Copyright (C) 2019 Niwla
-- 
-- traptest is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- traptest is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
-- 
-- You should have received a copy of the GNU Lesser General Public License
-- along with traptest. If not, see <http://www.gnu.org/licenses/>.

local S = minetest.get_translator(minetest.get_current_modname())

local current_checkpoint = {x=0,y=1,z=-52}
minetest.register_node("parkour:checkpoint", {
  description = S("Checkpoint"),
  drawtype = "plantlike",
	waving = 0,
	tiles = {"checkpoint.png"},
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	groups = {snappy = 5},

  on_punch = function(pos, node, player, pointed_thing)
    minetest.sound_play("plop", {
     to_player = player,
     gain = 2.0,
    })
    current_checkpoint = pos
  end,

})


minetest.register_on_respawnplayer(function(player)
  minetest.chat_send_all(player:get_player_name().." Ist gestorben!")
  minetest.after(0.01, function()
    if current_checkpoint ~= nil then
      player:set_pos(current_checkpoint)
    else
      player:set_pos({x=0,y=1,z=-52})
    end
end)
end)


minetest.register_node("parkour:endpoint", {
	description = S("Endpoint"),
	drawtype = "plantlike",
	waving = 0,
	tiles = {"finalpoint.png"},
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	groups = {snappy = 5,},

  on_punch = function(pos, node, player, pointed_thing)
          minetest.chat_send_all(player:get_player_name().." hat den parkour geschafft!")
          local idx = player:hud_add({
            hud_elem_type = "image",
            position      = {x = 0.5, y = 0.5},
            offset        = {x = 0,   y = 0},
            text          = "ende.png",
            alignment     = {x = 0, y = 0},  -- center aligned
            scale         = {x = 1.5, y = 1.5}, -- covered late
            number = 0xD61818,
          })

          minetest.sound_play("applause", {
           to_player = player,
           gain = 2.0,
          })
          minetest.after(5, function() player:hud_remove(idx) end)
   end

})


minetest.register_node("parkour:healer", {
	description = S("Healer"),
	drawtype = "plantlike",
	waving = 0,
	tiles = {"checkpoint.png^[colorize:#4A5A07:230"},
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	groups = {snappy = 5,},

  on_punch = function(pos, node, player, pointed_thing)
    minetest.sound_play("plop", {
     to_player = player,
     gain = 2.0,
    })
    player:set_hp(20)
  end,

})


minetest.register_node("parkour:killer", {
	description = S("KILLER"),
	drawtype = "plantlike",
	waving = 0,
	tiles = {"checkpoint.png^[colorize:#e11212:230"},
	paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
  groups = {snappy = 5,},
  damage_per_second = 20,




})

minetest.register_alias("parkour:endpoint", "ziel")

